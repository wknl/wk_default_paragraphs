# AppnoNL Default Paragraphs module

## Updating configuration

1. Get the latest version of the module.
2. Copy the configuration .yml files from the config/install folder to your config(/sync) folder.
3. If already in Git, run a diff to see what changed and nothing will break.
4. Run "drush cim" to import the new configuration. Never with -y, always check the files before importing.
